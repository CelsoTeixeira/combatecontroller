using UnityEngine;
using System.Collections;

public class CharacterMovement : MonoBehaviour
{

    private float moveSpeedMultiplier = 25;         //We need to multiply this by 25 because the avatar its to small...
    private float stationaryTurnSpeed = 180;
    private float movingTurnSpeed = 360;

    private bool onGround;

    private Animator _animator;
    private Rigidbody _rigidbody;

    private Vector3 moveInput;      //Move vector3
    private float turnAmount;       //pass to mecanin
    private float forwardAmount;    //pass to mecanin
    private Vector3 velocity;

    private float jumpPower = 10;

    private IComparer rayHitComparer;

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();

        SetupAnimator();
    }

    public void Move(Vector3 move)
    {
        if (move.magnitude > 1)
            move.Normalize();

        this.moveInput = move;

        velocity = _rigidbody.velocity;

        ConvertMoveInput();
        ApplyExtraTurnRotation();
        GroundCheck();
        UpdateAnimator();

        //Debug.Log(onGround);
    }

    void SetupAnimator()
    {
        _animator = GetComponent<Animator>();

        foreach (Animator inChild in GetComponentsInChildren<Animator>())
        {
            if (inChild != _animator)
            {
                _animator.avatar = inChild.avatar;
                Destroy(inChild);
                break;
            }
        }
    }

    void OnAnimatorMove()
    {
        if (onGround && Time.deltaTime > 0)
        {
            Vector3 v = (_animator.deltaPosition*moveSpeedMultiplier)/Time.deltaTime;

            v.y = _rigidbody.velocity.y;
            _rigidbody.velocity = v;
        }    
    }

    void ConvertMoveInput()
    {
        Vector3 localMove = transform.InverseTransformDirection(moveInput);

        turnAmount = Mathf.Atan2(localMove.x, localMove.z);
        forwardAmount = localMove.z;
    }

    void UpdateAnimator()
    {
        _animator.applyRootMotion = true;

        _animator.SetFloat("Forward", forwardAmount, 0.1f, Time.deltaTime);
        _animator.SetFloat("Turn", turnAmount, 0.1f, Time.deltaTime);
    }

    void ApplyExtraTurnRotation()
    {
        float turnSpeed = Mathf.Lerp(stationaryTurnSpeed, movingTurnSpeed, forwardAmount);
        transform.Rotate(0, turnAmount*turnSpeed*Time.deltaTime, 0);
    }

    void GroundCheck()
    {
        Ray ray = new Ray(transform.position + Vector3.up * .5f, -Vector3.up);

        RaycastHit[] hits = Physics.RaycastAll(ray, 0.5f);
        rayHitComparer = new RayHitComparer();

        System.Array.Sort(hits, rayHitComparer);

        //Debug.Log(velocity.y);

        if (velocity.y < jumpPower*0.5f)
        {
            onGround = false;
            _rigidbody.useGravity = true;

            foreach (var hit in hits)
            {
                if (!hit.collider.isTrigger)
                {
                    if (velocity.y <= 0)
                    {
                        _rigidbody.position = Vector3.MoveTowards(_rigidbody.position, hit.point, Time.deltaTime*5f);
                    }

                    onGround = true;
                    _rigidbody.useGravity = false;

                    break;
                }
            }
        }
    }

    //TODO: Move this to its own class.
    class RayHitComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            return ((RaycastHit) x).distance.CompareTo(((RaycastHit) y).distance);
        }
    }
}