using UnityEngine;
using System.Collections;

//TODO: Apply cursor state.
public class UserInput : MonoBehaviour
{
    public bool WalkByDefault = false;

    private CharacterMovement _characterMovement;
    private Transform camera;
    private Vector3 camForward;
    private Vector3 move;

    //private float cameraForward;
    private bool aim;
    private float aimingWeight;
    //private float cameraSpeedOffset;
    
    [Tooltip("A distancia da camera para a personagem.")]
    public float ZAxisNormal = -2f;                   
    [Tooltip("A distancia da camera para a personagem quando esta mirando")]  
    public float ZAxisAiming = -1f;

    public KeyCode RunningKeyCode = KeyCode.LeftShift;

    public KeyCode LookCloseKeyCode = KeyCode.F;

    void Start()
    {
        if (Camera.main != null)
        {
            camera = Camera.main.transform;
        }

        _characterMovement = GetComponent<CharacterMovement>();
    }

    void LateUpdate()
    {
        aim = Input.GetKey(LookCloseKeyCode);

        aimingWeight = Mathf.MoveTowards(aimingWeight, (aim) ? 1.0f : 0.0f, Time.deltaTime*5);
      
        Vector3 normalState = new Vector3(0, 0, ZAxisNormal);
        Vector3 aimintState = new Vector3(0, 0, ZAxisAiming);

        Vector3 pos = Vector3.Lerp(normalState, aimintState, aimingWeight);

        camera.transform.localPosition = pos;
    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        if (camera != null)
        {
            camForward = Vector3.Scale(camera.forward, new Vector3(1, 0, 1)).normalized;

            move = vertical*camForward + horizontal*camera.right;
        }
        else
        {
            move = vertical*Vector3.forward + horizontal*Vector3.right;
        }

        if (move.magnitude > 1)
            move.Normalize();


        bool walkToggle = Input.GetKey(RunningKeyCode) || aim;

        float walkMultiplier = 1;

        if (WalkByDefault)
        {
            if (walkToggle)
            {
                walkMultiplier = 1;
            }
            else
            {
                walkMultiplier = 0.5f;
            }
        }
        else
        {
            if (walkToggle)
            {
                walkMultiplier = 0.5f;
            }
            else
            {
                walkMultiplier = 1;
            }
        }

        move *= walkMultiplier;

        _characterMovement.Move(move);
    }
}