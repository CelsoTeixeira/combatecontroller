using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public int MaxLife = 6;

    public float DistanceToInteract = 4f;
    public KeyCode KeyToInteract = KeyCode.E;
    
    public bool OnCombat = false;
    public GameObject Target;


    private int _currentLife;
    public int CurrentLife { get { return _currentLife; } }

    public CompanionController _companionController;

    private GameControl _gameControl;

    void Awake()
    {
        Cursor.lockState = CursorLockMode.Confined;

        _currentLife = MaxLife;
        _companionController = GameObject.FindGameObjectWithTag("Companion").GetComponent<CompanionController>();
        _gameControl = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameControl>();
    }

    void Start()
    {
        _gameControl.ChangeState(GameControl.GameState.Normal);
    }

    void Update()
    {
        if (OnCombat)
        {
            _companionController.ChangeStance(Stance.COMBAT);
            
            if (Input.GetKeyDown(KeyToInteract))
            {
                Debug.Log("Getting core.");
                Target.GetComponent<EnemyController>().GetCore();
            }

            if (Input.GetMouseButtonDown(0))
            {
                //Attack
                Debug.Log("Normal attack.");
                _companionController.NormalAttack();
            }

            if (Input.GetMouseButtonDown(1))
            {
                //Intercept
                Debug.Log("Intercepting attack.");
                _companionController.Intercept();
            }
        }
        else
        {
            _companionController.ChangeStance(Stance.NONCOMBAT);
        }
    }

    public void EnterCombat(GameObject target)
    {
        Target = target;

        EnemyController enemyController = Target.GetComponent<EnemyController>();
        EnemyFSM enemyFsm = Target.GetComponent<EnemyFSM>();

        _companionController._target = Target;
        _companionController.RechargeAttack.ChangeTarget(Target, enemyController, enemyFsm);

        OnCombat = true;

        _gameControl.ChangeState(GameControl.GameState.Combat);
    }

    public void LeaveCombat()
    {
        Target = null;
        OnCombat = false;

        _gameControl.ChangeState(GameControl.GameState.Normal);
    }

    public void DoDamage(int damage)
    {
        _currentLife -= damage;

        Debug.Log("Current Life: " + _currentLife);

        if (_currentLife <= 0)
        {
            Debug.Log("Dead...");
        }
    }
}