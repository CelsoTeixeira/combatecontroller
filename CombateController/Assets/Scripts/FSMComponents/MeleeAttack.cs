using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class MeleeAttack
{
    public int Damage = 1;

    private bool _canAttack = true;
    public bool CanAttack { get { return _canAttack; } }

    public float Cooldown = 1;

    public float _timer;

    private PlayerController _playerController;
    private BaseMovement _baseMovement;

    public void Init(PlayerController playerController, BaseMovement baseMovement)
    {
        _playerController = playerController;
        _baseMovement = baseMovement;
    }

    public void Update(float delta)
    {
        if (!_canAttack)
        {
            CooldownUpdate(delta);
        }
    }

    private void CooldownUpdate(float delta)
    {
        _timer += delta;

        if (_timer >= Cooldown)
        {
            _canAttack = true;
            _timer = 0;
        }
    }
    
    public void Attack()
    {
        if (!_canAttack)
            return;

        //Debug.Log("Triggering melee attack!");

        PerformAttack();
    }

    private void PerformAttack()
    {
        _baseMovement.StopMovement();

        _canAttack = false;
        _timer = 0;

        //Do damage to player.
        _playerController.DoDamage((int)Damage);
    }
}