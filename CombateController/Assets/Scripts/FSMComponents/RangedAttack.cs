using System;
using UnityEngine;
using System.Collections;

public enum JumpStage
{
    Cooldown,
    Ready,
    WaitingToJump,
    Jumping,
}

//This is not the Ranged Attack, this is the jump!

[Serializable]
public class RangedAttack
{
    [Tooltip("Time to wait between each attack.")]
    public float Cooldown = 6;
    [Tooltip("The amount of damage done.")]
    public float Damage = 1;
    [Tooltip("The time we wait before we jump.")]
    public float TimeBeforeJump = 4f;

    public JumpStage Stage = JumpStage.Ready;

    private bool _canAttack = true;
    public bool CanAttack { get { return _canAttack; } }

    private bool _performingAttack = false;
    public bool PerformingAttack { get { return _performingAttack;} }

    private bool _waitingToJump = false;
    public bool WaitingToJump { get { return _waitingToJump; } }

    public float DistanceToStopJump = 2.0f;

    public Action OnFinishAttackCallback = () => {};
    public Action OnReadyCallback = () => {};
    public Action OnJumpCallback = () => {};

    private float _timer = 0;
    public float _timerBeforeJump = 0;

    private PlayerController _playerController;

    public void Init(PlayerController playerController)
    {
        _playerController = playerController;
    }

    public void Update(float delta, float distance, GameObject go, Vector3 target, BaseMovement mov)
    {
        if (!_canAttack)
        {
            CooldownUpdate(delta);
        }

        CheckDistanceForAttack(distance);
        WaitBeforeJump(delta, target, go);
    }

    private void CooldownUpdate(float delta)
    {
        if (_canAttack)
        {
            Stage = JumpStage.Ready;
            return;
        }

        _timer += delta;

        Stage = JumpStage.Cooldown;

        if (_timer >= Cooldown)
        {
            _canAttack = true;
        
            Stage = JumpStage.Ready;

            _timerBeforeJump = 0;       //Reset the timer.

            if (OnReadyCallback != null)
                OnReadyCallback.Invoke();
        }
    }

    private void LookAt(Vector3 position, GameObject go)
    {
        go.transform.LookAt(position);
    }

    private void WaitBeforeJump(float delta, Vector3 position, GameObject go)
    {
        if (!_waitingToJump)
            return;
        
        _timerBeforeJump += delta;

        LookAt(position, go);

        Stage = JumpStage.WaitingToJump;

        if (_timerBeforeJump >= TimeBeforeJump)
        {
            _waitingToJump = false;  //We're done now.

            _performingAttack = true;   //We're starting do check for distance.
            
        }
    }

    private void CheckDistanceForAttack(float distance)
    {
        if (!PerformingAttack)
            return;

        Stage = JumpStage.Jumping;

        if (OnJumpCallback != null)
            OnJumpCallback.Invoke();

        if (distance <= DistanceToStopJump)
        {
            //Apply damage
            //Debug.Log("Attack!");
            _playerController.DoDamage((int)Damage);

            if (OnFinishAttackCallback != null)
                OnFinishAttackCallback.Invoke();   //This is called when we're done with the attack.

            _performingAttack = false;      //We're done with the attack.

            _canAttack = false;     //We can attack again.
            _timer = 0;     //Reset the cooldown timer.
            
        }
    }

    public void CancelJump()
    {
        _performingAttack = false;

        _canAttack = false;     //We can attack again.
        _timer = 0;     //Reset the cooldown timer.
        _timerBeforeJump = 0;

        Stage = JumpStage.Cooldown;
    }

    public void Attack()
    {
        if (!_canAttack)
            return;

        //Debug.Log("Triggering jump!");

        PerformAttack();
    }

    private void PerformAttack()
    {
        //Jump to player, then do damage.
        //_performingAttack = true;
        _waitingToJump = true;
    }

}