using UnityEngine;
using System.Collections;

public enum MovementState
{
    NONE = 0,
    MOVING = 1,
    STOPPED = 2
}

public class BaseMovement : MonoBehaviour
{
    public float DistanceToStop = 3f;
    
    public MovementState MovState = MovementState.NONE;

    protected Vector3 _destination;
    protected NavMeshAgent _agent;

    protected virtual void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();               
    }

    protected virtual void Update()
    {
        StateController();

        if (_destination == null)
            return;

        //Debug.Log(Vector3.Distance(this.transform.position, _destination.position));

        if (Vector3.Distance(this.transform.position, _destination) <= DistanceToStop)
        {
            //Debug.Log("Agent stop");
            _agent.Stop();
        }

        MovementDebug();
    }

    protected virtual void StateController()
    {
        if (_agent.velocity != Vector3.zero)
        {
            MovState = MovementState.MOVING;
        }
        else
        {
            MovState = MovementState.STOPPED;
        }
    }

    protected virtual void MovementDebug()
    {
#if UNITY_EDITOR

        Debug.DrawLine(this.transform.position, _destination, Color.red);

#endif        
    }

    public void ChangeSpeed(float newSpeed)
    {
        _agent.speed = newSpeed;
    }

    public void StopMovement()
    {
        _agent.Stop();
        _agent.velocity = Vector3.zero;
    }

    public virtual void DoMovement(Vector3 positionToGo)
    {
        //Debug.Log("Doing new movement.");

        _destination = positionToGo;

        _agent.Stop();
        _agent.SetDestination(_destination);
        _agent.Resume();
    }
}