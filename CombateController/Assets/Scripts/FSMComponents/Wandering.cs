using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

[Serializable]
public class Wandering
{
    public Transform Center;
    public float Radius = 5;

    private float _timeToWait;
    public Vector2 RangeTimeToWait = new Vector2(5, 10);

    private float _timer = 0;
    private bool _canAcquireNewTarget = true;

    private BaseMovement _baseMovement;

    public void Init(BaseMovement mov)
    {
        _baseMovement = mov;
    }

    public void WandererUpdate(float delta)
    {
        MoveAround();
        CooldownTimer(delta);
    }

    private void MoveAround()
    {
        if (_baseMovement.MovState == MovementState.STOPPED)
        {
            if (_canAcquireNewTarget)
            {
                _baseMovement.DoMovement(AcquireTarget());
                _canAcquireNewTarget = false;
            }
        }    
    }

    private void CooldownTimer(float delta)
    {
        _timer += delta;

        if (_timer >= _timeToWait)
        {
            _canAcquireNewTarget = true;
        }
    }

    //This is just getting a random target based on a epicenter and a radius. 
    //This is wrong someway.
    private Vector3 AcquireTarget()
    {
        Vector3 newTarget = new Vector3();
        newTarget.x = Center.position.x + Random.Range(-Radius, Radius);
        newTarget.z = Center.position.y + Random.Range(-Radius, Radius);
        newTarget.y = 0;
        
        //Debug.Log("Trying to find new target: " + newTarget);

        _timeToWait = Random.Range(RangeTimeToWait.x, RangeTimeToWait.y);

        _timer = 0;

        return newTarget;
    }


}