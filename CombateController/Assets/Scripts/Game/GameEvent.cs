using System;
using UnityEngine;
using System.Collections;

public class GameEvent
{
    private Action Update;

    public void Subscribe(Action action)
    {
        Update += action;
    }

    public void UnSubscribe(Action action)
    {
        Update -= action;
    }

    public void Broadcast()
    {
        if (Update != null)
        {
            Update.Invoke();
        }
    }
}