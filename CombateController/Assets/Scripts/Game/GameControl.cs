using UnityEngine;
using System.Collections;

public class GameControl : MonoBehaviour
{
    public enum GameState
    {
        Normal,
        Combat,
    }

    public GameState State = GameState.Normal;

    private SoundControl _soundControl;

    void Awake()
    {
        _soundControl = GetComponent<SoundControl>();
    }

    public void ChangeState(GameState state)
    {
        State = state;

        //_soundControl.ChangeSound(State);
    }
}