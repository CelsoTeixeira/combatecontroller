using UnityEngine;
using System;
using System.Collections;

public class EnemyFSM : MonoBehaviour
{
    public float NormalSpeed = 2f;
    public float JumpSpeed = 6f;

    public float DistanceToAttackRanged = 6f;
    public float DistanceToAttackMelee = 1f;
    
    public GameObject Target;

    public RangedAttack RangedAttack = new RangedAttack();
    public MeleeAttack MeleeAttack = new MeleeAttack();

    public bool IsStunned = false;
    public float StunTime = 2f;

    public Action OnStunCallback = () => {};        //This is called when we get stuned.

    private float _stunTimer = 0;

    private BaseMovement _baseMovement;
    private Rigidbody _rigidbody;
    private PlayerController _playerController;

    void Awake()
    {
        Target = GameObject.FindGameObjectWithTag("Player");

        _playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        _baseMovement = GetComponent<BaseMovement>();
        _rigidbody = GetComponent<Rigidbody>();

        RangedAttack.Init(_playerController);
        MeleeAttack.Init(_playerController, _baseMovement);

        RangedAttack.OnFinishAttackCallback += AfterJumpAttack;
    }

    //This is called from the EnemyController
    public void FSMUpdate(float distance, float delta, Stance stance)
    {
        //Debug.Log(distance);

        //Debug.Log(_rigidbody.velocity);

        //Clean this part.
        if (IsStunned)
        {
            _stunTimer += delta;

            if (_stunTimer >= StunTime)
            {
                IsStunned = false;
                _stunTimer = 0;
            }            
        }
        else if (!IsStunned)
        {
            //Debug.Log("Not stunned, doing update stuff.");

            RangedAttack.Update(delta, distance, this.gameObject, Target.transform.position, _baseMovement);
            MeleeAttack.Update(delta);

            _rigidbody.velocity = Vector3.zero;
        }
         
        if (IsStunned || stance == Stance.DEAD)
            return;
              
        //Get a proper control here.
        if (RangedAttack.WaitingToJump)
        {
            _baseMovement.StopMovement();
        }
        else if (RangedAttack.PerformingAttack)
        {
            _baseMovement.ChangeSpeed(JumpSpeed);
            _baseMovement.DoMovement(Target.transform.position);
        }
        else if (!RangedAttack.PerformingAttack)
        {
            _baseMovement.ChangeSpeed(NormalSpeed);
        }

        //FSM Changer, move this to a proper method and implement a ENUM to be the main indicator here.
        //Add one method to change status and add Event calls when we change it.
        if (distance <= DistanceToAttackRanged && distance > DistanceToAttackMelee && RangedAttack.CanAttack)
        {
            RangedAttack.Attack();
        }
        else if (distance < DistanceToAttackMelee && MeleeAttack.CanAttack && !RangedAttack.PerformingAttack && RangedAttack.WaitingToJump)
        {
            MeleeAttack.Attack();
        }
        else if(distance > DistanceToAttackMelee && !RangedAttack.PerformingAttack && !RangedAttack.WaitingToJump)
        {
            _baseMovement.DoMovement(Target.transform.position);
        }
        else if (distance < DistanceToAttackMelee && !RangedAttack.PerformingAttack && !RangedAttack.WaitingToJump)
        {
            _baseMovement.StopMovement();
        }

        if (distance <= 1.0f)
        {
            _baseMovement.StopMovement();
        }
    }

    public void Stun()
    {
        Debug.Log("Stun!");

        if (OnStunCallback != null)
            OnStunCallback.Invoke();

        IsStunned = true;

        RangedAttack.CancelJump();
        _baseMovement.StopMovement();        
        
    }

    public void KnockBack()
    {
        this.transform.position = this.transform.position + Vector3.back;
    }

    //This is called from the RangedAttack callback action.
    private void AfterJumpAttack()
    {
        _baseMovement.StopMovement();
    }
    

}