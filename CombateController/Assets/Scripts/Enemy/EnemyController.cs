using UnityEngine;
using System.Collections;



public class EnemyController : MonoBehaviour
{
    public int MaxLife = 3;
    private int _currentLife = 0;

    public float DeadRechargeTime = 10f;
    private float _deadRechargeTimer = 0;

    private bool _haveCore = true;
    public bool HaveCore { get { return _haveCore; } }

    public Wandering WanderState = new Wandering();

    public GameObject Target;

    public float DistanceToCombat = 10f;

    public Stance Stance = Stance.NONE;

    public Material NormalColor;
    public Material DeadColor;

    private BaseMovement _baseMovement;
    private EnemyFSM _enemyFsm;

    private PlayerController _playerController;

    private Renderer _renderer;

    void Awake()
    {
        if (Target == null)
            Target = GameObject.FindGameObjectWithTag("Player");

        _baseMovement = GetComponent<BaseMovement>();
        _enemyFsm = GetComponent<EnemyFSM>();
        _renderer = GetComponent<Renderer>();


        _playerController = Target.GetComponent<PlayerController>();

        WanderState.Init(_baseMovement);

        _currentLife = MaxLife;
    }

    void Update()
    {
        float distance = Vector3.Distance(this.transform.position, Target.transform.position);

        StanceControl(distance);

        switch (Stance)
        {
            case Stance.NONCOMBAT:
                WanderState.WandererUpdate(Time.deltaTime);
                break;

            case Stance.COMBAT:
                _enemyFsm.FSMUpdate(distance, Time.deltaTime, Stance);
                break;
                
            case Stance.DEAD:
                DeadRecharge();
                break;
        }

        //Move this to a proper change stance.
        if (Stance == Stance.DEAD)
        {
            _renderer.material = DeadColor;
        }
        else
        {
            _renderer.material = NormalColor;
        }
    }

    public void DoDamage(int damage)
    {
        _currentLife -= damage;

        if (_currentLife <= 0)
        {
            Stance = Stance.DEAD;
            _deadRechargeTimer = 0;

            _baseMovement.StopMovement();
        }
    }

    public void GetCore()
    {
        if (Stance != Stance.DEAD)
            return;

        _haveCore = false;
        _playerController.LeaveCombat();

        Destroy(this.gameObject);
    }

    private void DeadRecharge()
    {
        _deadRechargeTimer += Time.deltaTime;

        if (_deadRechargeTimer >= DeadRechargeTime)
        {
            Stance = Stance.NONCOMBAT;

            _deadRechargeTimer = 0f;
        }
    }

    private void StanceControl(float distance)
    {
        if (Stance == Stance.DEAD)
            return;

        if (distance <= DistanceToCombat)
        {
            _playerController.EnterCombat(this.gameObject);
            Stance = Stance.COMBAT;
        }
        else if (Stance != Stance.COMBAT)
        {
            Stance = Stance.NONCOMBAT;
        }
    }
}