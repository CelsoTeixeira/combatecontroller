using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Stance
{
    NONE = 0,
    NONCOMBAT = 1,
    COMBAT = 2,
    DEAD = 3,
}

public class CompanionController : MonoBehaviour 
{
    public Stance Stance = Stance.NONE;

    public float MaxDistanceFromPlayer = 10f;

    public RechargeAttack RechargeAttack = new RechargeAttack();
    
    private CompanionMovement _baseMovement;

    public GameObject _target;
    public FreeCameraLook FreeCameraLook;
    public GameObject Player;

    private bool GoToPosition = false;
    
    private GameObject go;

    void Awake()
    {
        _baseMovement = GetComponent<CompanionMovement>();
        FreeCameraLook = GameObject.FindGameObjectWithTag("MainCamera").GetComponentInParent<FreeCameraLook>();
        Player = GameObject.FindGameObjectWithTag("Player");

        go = this.gameObject;

        RechargeAttack.Init(_baseMovement, this.gameObject);
    }

    void Update()
    {
        switch (Stance)
        {
            case Stance.NONCOMBAT:

                _baseMovement.StartFollowing();                                 //Follow the player.
                _baseMovement.ChangeSpeed(_baseMovement.AgentSpeed);            //Reset the velocity.

                if (_baseMovement.MovState == MovementState.STOPPED)            //If we're near the player, look for the same spot of the camera.
                {
                    Debug.Log("Looking for camera target.");
                    go.transform.localRotation = Quaternion.Euler(0, FreeCameraLook.lookAngle, 0);        //This will make the companion look to the same position we're looking.
                    //this.transform.Rotate(new Vector3(0, FreeCameraLook.lookAngle, 0));
                }

                break;

            case Stance.COMBAT:

                float distance = Vector3.Distance(this.transform.position, _target.transform.position);
                float distanceToPlayer = Vector3.Distance(this.transform.position, Player.transform.position);

                this.gameObject.transform.LookAt(_target.transform.position);   //Look at the target.

                if (RechargeAttack.IsIntercepting)
                {
                    Debug.Log("Doing intercept");
                    RechargeAttack.UpdateIntercepting(distance);                    //Update class
                    _baseMovement.DoMovement(_target.transform.position);
                }
                else if (MaxDistanceFromPlayer <= MaxDistanceFromPlayer && !GoToPosition && !RechargeAttack.IsIntercepting)
                {
                    Debug.Log("Stopped");

                    _baseMovement.StopFollowing();                                  //Stop the movement
                    
                    Debug.DrawLine(this.gameObject.transform.position, Player.transform.position, Color.green);
                }
                else if (!RechargeAttack.IsIntercepting && distanceToPlayer > MaxDistanceFromPlayer)
                {
                    Debug.Log("Moving");

                    GoToPosition = true;
                    _baseMovement.StartFollowing(); //Go near the player again.
                    Debug.DrawLine(this.gameObject.transform.position, Player.transform.position, Color.red);
                }

                if (GoToPosition)
                {
                    Debug.Log("Going to position");

                    if (distance <= 4.0f)
                    {
                        GoToPosition = false;
                    }
                }

                RechargeAttack.Update(Time.deltaTime);      //Recharge the attack.
                

                break;
        }        
    }

    public void ChangeStance(Stance newStance)
    {
        Stance = newStance;        
    }

    public void NormalAttack()
    {
        RechargeAttack.Attack();
    }

    public void Intercept()
    {
        RechargeAttack.Intercept();
    }
}