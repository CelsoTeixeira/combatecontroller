using UnityEngine;
using System.Collections;

public class CompanionMovement : BaseMovement
{
    public float AgentSpeed = 2f;

    public bool Follow = true;

    public Transform Target;

    protected override void Awake()
    {
        base.Awake();

        Target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

        if (Target != null)
            _destination = Target.position;

        _agent.SetDestination(_destination);
    }

    protected override void Update()
    {
        _agent.speed = AgentSpeed;

        _destination = Target.position;

        StateController();

        //Debug.Log(Vector3.Distance(this.transform.position, _destination.position));

        if (Vector3.Distance(this.transform.position, _destination) <= DistanceToStop)
        {
            //Debug.Log("Agent stop");
            _agent.Stop();
        }
        else
        {
            if (Follow)
            {
                //Debug.Log("Agent resume");
                _destination = Target.position;
                _agent.SetDestination(Target.position);
                _agent.Resume();
            }
        }    
            
        MovementDebug();
    }

    public void StopFollowing()
    {
        Follow = false;
    }

    public void StartFollowing()
    {
        Follow = true;
    }

    protected override void MovementDebug()
    {
        Debug.DrawLine(this.transform.position, _destination, Color.green);
    }
}