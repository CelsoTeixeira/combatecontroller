using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
    public int Damage;

    public float SelfDestroy = 10f;

    private Rigidbody _rigidbody;

    private Vector3 Target;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();

        Destroy(this.gameObject, SelfDestroy);
    }

    void Update()
    {
        this.transform.position = Vector3.MoveTowards(transform.position, Target, 5*Time.deltaTime);

        float distance = Vector3.Distance(transform.position, Target);

        if (distance <= 0.1f)
        {
            Destroy(this.gameObject);
        }
    }

    public void ApplyForce(Vector3 force)
    {
        //_rigidbody.AddForce(force, ForceMode.Impulse);
    }

    public void SetTarget(Vector3 position)
    {
        Target = position;
    }

    public void OnTriggerEnter(Collider coll)
    {
        if (coll.CompareTag("Enemy"))
        {
            Debug.Log("Hit enemy");

            EnemyController enemyController = coll.GetComponent<EnemyController>();
            enemyController.DoDamage(Damage);

            Destroy(this.gameObject);
        }
    }
}
