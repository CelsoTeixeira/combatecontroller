using System;
using UnityEngine;
using System.Collections;

public enum AttackStatus
{
    Recharging = 1,
    FullyCharge = 2,
}

[Serializable]
public class RechargeAttack
{
    public AttackStatus Status = AttackStatus.Recharging;

    public int Damage = 3;
    public float RechargeTime = 3f;

    public float InterceptSpeed = 6f;
    
    private bool _isIntercepting = false;
    public bool IsIntercepting { get { return _isIntercepting; } }

    private float _timer = 0;
    private float _rechargeTimer = 0;

    public ParticleSystem RechargingParticleSystem;
    public ParticleSystem CompleteParticleSystem;

    public GameObject BulletPrefab;
    public Transform MuzzleTransform;

    public GameObject _target;
    public EnemyController _enemyController;
    public EnemyFSM _enemyFsm;

    private GameObject myGameObject;
    private CompanionMovement _companionMovement;

    public void Init(CompanionMovement companionMovement, GameObject go)
    {
        _companionMovement = companionMovement;
        myGameObject = go;
    }

    public void ChangeTarget(GameObject target, EnemyController enemyController, EnemyFSM enemyFsm)
    {
        _target = target;
        _enemyController = enemyController;
        _enemyFsm = enemyFsm;
    }

    public void Update(float delta)
    {
        if (!IsIntercepting)
        {
            CooldownUpdate(delta);
        }
        
        ChargeAttack(delta);        
    }

    public void UpdateIntercepting(float distance)
    {
        if (IsIntercepting)
        {            
            Intercepting(distance);
        }
    }

    private void Intercepting(float distance)
    {
        if (!IsIntercepting)
            return;

        _enemyFsm.Stun();

        _companionMovement.DoMovement(_target.transform.position);  //move to the target.
        _companionMovement.ChangeSpeed(InterceptSpeed);     //Change the speed.

        _isIntercepting = true;

        if (distance <= 1)
        {
            _companionMovement.StopMovement();
            _isIntercepting = false;
            Status = AttackStatus.Recharging;
            _timer = 0;
            _rechargeTimer = 0;

            _enemyFsm.KnockBack();
        }
    }

    private void CooldownUpdate(float delta)
    {
        _timer += delta;

        if (_timer >= RechargeTime)
        {
            Status = AttackStatus.Recharging;
            _timer = 0;
        }
    }
    
    private void ChargeAttack(float delta)
    {
        if (Status == AttackStatus.FullyCharge)
            return;

        if (IsIntercepting)
            return;

        _rechargeTimer += delta;

        Status = AttackStatus.Recharging;

        if (!RechargingParticleSystem.isPlaying)
        {
            RechargingParticleSystem.Play();

            RechargingParticleSystem.startSpeed = -.5f;
            RechargingParticleSystem.emissionRate = 25f;
        }

        if (RechargingParticleSystem.isPlaying)
        {
            float particleSpeed = Mathf.Clamp(_rechargeTimer, -5, 2);
            particleSpeed *= -1;

            float particles = 25*(_rechargeTimer + 1);

            RechargingParticleSystem.startSpeed = particleSpeed;
            RechargingParticleSystem.emissionRate = particles;
        }

        if (_rechargeTimer >= RechargeTime)
        {
            Status = AttackStatus.FullyCharge;
            RechargingParticleSystem.Stop();
            CompleteParticleSystem.Play();
        }
    }

    public void Attack()
    {
        
        if (Status == AttackStatus.FullyCharge)
        {
            Debug.Log("Full attack.");

            //Kill enemy
            //_enemyController.DoDamage(_enemyController.MaxLife);

            Vector3 forward = myGameObject.transform.forward;

            GameObject temp = GameObject.Instantiate(BulletPrefab, MuzzleTransform.position, Quaternion.identity) as GameObject;
            Bullet tempBullet = temp.GetComponent<Bullet>();
            tempBullet.Damage = _enemyController.MaxLife;
            tempBullet.ApplyForce(forward * 10);
            tempBullet.SetTarget(_target.transform.position);

            _rechargeTimer = 0;
        }
        else
        {
            Debug.Log("Partial attack");

            //Do one damage
            //_enemyController.DoDamage(Damage);

            Vector3 forward = myGameObject.transform.forward;

            GameObject temp = GameObject.Instantiate(BulletPrefab, MuzzleTransform.position, Quaternion.identity) as GameObject;
            Bullet tempBullet = temp.GetComponent<Bullet>();
            tempBullet.Damage = Damage;
            tempBullet.ApplyForce(forward * 10);
            tempBullet.SetTarget(_target.transform.position);

            _rechargeTimer = 0;
        }
    }

    public void Intercept()
    {
        if (Status == AttackStatus.FullyCharge)
        {
            Debug.Log("Intercpt!");

            _isIntercepting = true;
        }
        else
        {
            Debug.Log("Cant intercept");
        }
    }

}